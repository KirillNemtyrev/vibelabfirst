package org.example;

public class Main {

    private static final int recordBook = 0x21f75;
    private static final long number = 89123456789L;
    private static final int lastTwo = 0b1011001;
    private static final int lastFour = 015205;

    public static void main(String[] args) {
        int value = (10 + 1) % 26;
        System.out.println(value);
        System.out.println((char) (value+'A'));
    }
}